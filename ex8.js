    const mypromise =  new Promise((resolve,reject)=>{
        setTimeout(()=>{
            console.log("I am just promise outside without")
            let con =true;
            if(con){
                resolve("done");
            }
            else{
                reject("error");
            }
            
        },1000)
    })

    // Use of async await concept using promises

const a = () => {
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            console.log("I am a")
            let con =true;
            if(con){
                resolve("done");
            }
            else{
                reject("error");
            }
            
        },3000)
    })
}

const b = () => {
    setTimeout(() => {
        console.log("I am b")
    },2000)
}

async function start(){
    const val = await mypromise
    console.log(val);
    await a()
    b();
}
start();
// a()
// b()
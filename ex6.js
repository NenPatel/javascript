const a = (callback) => {
    setTimeout(()=>{
        console.log("I am a")
        callback()
    },3000) 
}

// callback function concept is used to execute a function in sequence

const b = () => {
    setTimeout(() => {
        console.log("I am b")
    },2000)
}

// async function start(){
//     await a()
//     b();
// }
// start();
a(b)
// b()
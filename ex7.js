const a = () => {
    setTimeout(()=>{
        console.log("I am a")
    },3000) 
}

// setTimeout is asynchronous function use asynchronous concept

const b = () => {
    setTimeout(() => {
        console.log("I am b")
    },2000)
}

// async function start(){
//     await a()
//     b();
// }
// start();
a()
b()
// Using the concept of Promises and .then() and .catch() method

const a = () => {
return new Promise((resolve,reject)=>{
    setTimeout(()=>{
        console.log("I am a")
        let con =true;
        if(con){
            resolve("done");
        }
        else{
            reject("error");
        }
        
    },3000)
})
}

const b = () => {
setTimeout(() => {
    console.log("I am b")
},2000)
}

a().then(b).catch(err => console.log(err))